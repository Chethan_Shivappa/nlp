# How to create a new environment and add ipykernel

step1: conda create -n env-name python=version

step2: conda activate env-name

(Note): if ipykernel is not present then add pip install ipykernel or conda install ipykernel
        **Important : environment has to be activated**
        
step3:  ipython kernel install --user --name env-name
                    OR
        python -m ipykernel install --user --name myenv --display-name "kernel-name"

Note: env-name and kernel-name can be same or different.

(For eg: if the environment name is machine-learning and kernel name is ML)

step1: conda create -n machine-learning python=3.7
step2: conda activate machine-learning
step3: ipython kernel install --user --name ML
       python -m ipykernel install --user --name myenv --display-name "ML"



Install spacy and download language pack:
step 1: Activate the virtual_env
step 2: pip install spacy
step 3: python -m spacy download en_core_web_lg







